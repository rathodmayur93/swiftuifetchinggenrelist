//
//  GenresModel.swift
//  MovieDB
//
//  Created by ds-mayur on 6/23/19.
//  Copyright © 2019 ds-mayur. All rights reserved.
//

import SwiftUI

public struct GenresModel: Codable {
    
    public var genres   : [GenresListModel]
}

public struct GenresListModel: Codable, Identifiable{
    
    public var id   : Int
    public var name : String
}
