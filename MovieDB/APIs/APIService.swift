//
//  APIService.swift
//  MovieDB
//
//  Created by ds-mayur on 6/23/19.
//  Copyright © 2019 ds-mayur. All rights reserved.
//

import Foundation

protocol APIService{
    
    func fetchGenres(from endpoint : Endpoints, param: [String : String]?, successHandler: @escaping(_ response: GenresModel) -> Void, errorHandler: @escaping(_ error : Error) -> Void)
}

public enum Endpoints: String{
    
    case genreList = "/genre/movie/list"
    
}

public enum APIErros: Error{
    case apiError
    case invalidEndpoint
    case invalidResponse
    case noData
    case serializationError
}
