//
//  APICalls.swift
//  MovieDB
//
//  Created by ds-mayur on 6/23/19.
//  Copyright © 2019 ds-mayur. All rights reserved.
//

import Foundation
import Alamofire

class APICalls: APIService{
    
    public static let shared    = APICalls()
    private let apiKey          = "YOUR API KEY"
    private let baseUrl         = "https://api.themoviedb.org/3"
    private let language        = "en-US"
    private let urlSession      = URLSession.shared
    
    private let jsonDecoder: JSONDecoder = {
        let jsonDecoder = JSONDecoder()
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        return jsonDecoder
    }()
    
    //MARK: API Service Methods 2
    func fetchGenres(from endpoint: Endpoints, param: [String : String]?, successHandler: @escaping (GenresModel) -> Void, errorHandler: @escaping (Error) -> Void) {
        
        //Creating the URLComponents of the genres API
        guard var urlComponents = URLComponents(string: "\(baseUrl)\(endpoint.rawValue)") else{
            errorHandler(APIErros.invalidEndpoint)
            return
        }
        
        //Appending the parameters required for making an request.
        var queryItems = [URLQueryItem(name: "api_key", value: apiKey),
                          URLQueryItem(name: "language", value: language)]
        if let params = param{
            queryItems.append(contentsOf: params.map { URLQueryItem(name: $0.key, value: $0.value) })
        }
        
        //Appending the query parameters to the url using URLComponents
        urlComponents.queryItems = queryItems
        
        //Fetching the URL with parameter from the URLComponents
        guard let url = urlComponents.url else{
            errorHandler(APIErros.invalidEndpoint)
            return
        }
        
        Alamofire.request(url,
                          method: .get,
                          parameters: nil).response
            { (response) in
                
                if response.error != nil {
                    self.handleError(errorHandler: errorHandler, error: APIErros.apiError)
                    return
                }
                
                guard let httpResponse = response.response, 200..<300 ~= httpResponse.statusCode else {
                    self.handleError(errorHandler: errorHandler, error: APIErros.invalidResponse)
                    return
                }
                
                guard let responseData = response.data else {
                    self.handleError(errorHandler: errorHandler, error: APIErros.noData)
                    return
                }
                
                do {
                    let moviesResponse = try self.jsonDecoder.decode(GenresModel.self, from: responseData)
                    DispatchQueue.main.async {
                        successHandler(moviesResponse)
                    }
                } catch {
                    self.handleError(errorHandler: errorHandler, error: APIErros.serializationError)
                }
                            
        }
    }
    
    private func handleError(errorHandler: @escaping(_ error: Error) -> Void, error: Error) {
        DispatchQueue.main.async {
            errorHandler(error)
        }
    }
    
}
