//
//  GenresData.swift
//  MovieDB
//
//  Created by ds-mayur on 6/23/19.
//  Copyright © 2019 ds-mayur. All rights reserved.
//

import Combine
import SwiftUI

class GenresData : BindableObject{
    
    //didChange parameter will pass it to the UI
    let didChange           = PassthroughSubject<GenresData, Never>()
    private let apiService : APIService
    
    
    //Variables
    var isLoading: Bool = false{
        didSet{
            //Updating the UI since model has been updated
            didChange.send(self)
        }
    }
    
    //Variable
    var genreList : [GenresModel] = [] {
        didSet{
            //Updating the UI since model has been updated
            didChange.send(self)
        }
    }
    
    //Initializers
    init(apiService : APIService, endPoints : Endpoints) {
        self.apiService = apiService
        self.loadGenresList(endPoints)
    }
    
    //Making an api call to fetch the genres from the server
    func loadGenresList(_ endPoints : Endpoints){
        
        let group           = DispatchGroup()   // Creating GCC Group
        let queue           = DispatchQueue.global(qos: .background) // Running the queue in background
        var genreListValue  = [GenresModel]()
        
        //We made the api call since isLoading will become true
        isLoading = true
        
        //Making the asyncronous api call
        queue.async(group: group){
         
            group.enter()
            
            self.apiService.fetchGenres(from: endPoints, param: nil, successHandler: { (response) in
                print("Genre List \(response)")
                genreListValue.append(response)
                group.leave()
            }) { (error) in
                print(error.localizedDescription)
                group.leave()
            }
        }
        
        group.notify(queue: DispatchQueue.main){
            //Updating the isLoading flag
            self.isLoading = false
            //Sorting the result using id
            genreListValue[0].genres.sort{ $0.id < $1.id }
            //Assigning the value to the genreList
            self.genreList = genreListValue
        }
    }
}
