//
//  ContentView.swift
//  MovieDB
//
//  Created by ds-mayur on 6/23/19.
//  Copyright © 2019 ds-mayur. All rights reserved.
//

import SwiftUI

struct ContentView : View {
    
    @EnvironmentObject private var genreListValues: GenresData
    
    var body: some View {
        NavigationView{
            List{
                /*
                 - If genre list count is non-zero then loop through the list and set the text in list
                 */
                if genreListValues.genreList.count != 0{
                    //Loop throught the genreList to fetch the genre name
                    ForEach(genreListValues.genreList[0].genres){ genres in
                        //Setting the text
                        Text(genres.name)
                    }
                }
            }.navigationBarTitle(Text("Generes"))
        }
    }
}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        ContentView()
        .environmentObject(GenresData(apiService: APICalls.shared, endPoints: Endpoints.genreList))
        .colorScheme(.dark)
    }
}
#endif
